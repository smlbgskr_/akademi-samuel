/*
 Navicat Premium Data Transfer

 Source Server         : postgres
 Source Server Type    : PostgreSQL
 Source Server Version : 120011
 Source Host           : localhost:5432
 Source Catalog        : akademi-samuel
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 120011
 File Encoding         : 65001

 Date: 04/07/2022 13:08:08
*/


-- ----------------------------
-- Sequence structure for roles_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."roles_id_seq";
CREATE SEQUENCE "public"."roles_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for users_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."users_id_seq";
CREATE SEQUENCE "public"."users_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Table structure for data_nilai
-- ----------------------------
DROP TABLE IF EXISTS "public"."data_nilai";
CREATE TABLE "public"."data_nilai" (
  "dosen_id" int8 NOT NULL,
  "mata_kuliah_id" int8 NOT NULL,
  "nim" int8 NOT NULL,
  "keterangan" varchar(255) COLLATE "pg_catalog"."default",
  "nilai" int4
)
;

-- ----------------------------
-- Table structure for dosen
-- ----------------------------
DROP TABLE IF EXISTS "public"."dosen";
CREATE TABLE "public"."dosen" (
  "dosen_id" int8 NOT NULL,
  "nama_dosen" varchar(255) COLLATE "pg_catalog"."default",
  "user_id" int8 NOT NULL
)
;

-- ----------------------------
-- Records of dosen
-- ----------------------------
INSERT INTO "public"."dosen" VALUES (2, NULL, 2);

-- ----------------------------
-- Table structure for mahasiswa
-- ----------------------------
DROP TABLE IF EXISTS "public"."mahasiswa";
CREATE TABLE "public"."mahasiswa" (
  "nim" int8 NOT NULL,
  "alamat" varchar(255) COLLATE "pg_catalog"."default",
  "jurusan" varchar(255) COLLATE "pg_catalog"."default",
  "nama_mahasiswa" varchar(255) COLLATE "pg_catalog"."default",
  "tanggal_lahir" timestamp(6),
  "user_id" int8 NOT NULL
)
;

-- ----------------------------
-- Records of mahasiswa
-- ----------------------------
INSERT INTO "public"."mahasiswa" VALUES (3, NULL, NULL, NULL, NULL, 3);

-- ----------------------------
-- Table structure for mata_kuliah
-- ----------------------------
DROP TABLE IF EXISTS "public"."mata_kuliah";
CREATE TABLE "public"."mata_kuliah" (
  "mata_kuliah_id" int8 NOT NULL,
  "nama_mata_kuliah" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "nim" int8
)
;

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS "public"."roles";
CREATE TABLE "public"."roles" (
  "id" int4 NOT NULL DEFAULT nextval('roles_id_seq'::regclass),
  "name" varchar(20) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO "public"."roles" VALUES (1, 'ROLE_MAHASISWA');
INSERT INTO "public"."roles" VALUES (3, 'ROLE_DOSEN');
INSERT INTO "public"."roles" VALUES (2, 'ROLE_ADMIN');

-- ----------------------------
-- Table structure for user_roles
-- ----------------------------
DROP TABLE IF EXISTS "public"."user_roles";
CREATE TABLE "public"."user_roles" (
  "user_id" int8 NOT NULL,
  "role_id" int4 NOT NULL
)
;

-- ----------------------------
-- Records of user_roles
-- ----------------------------
INSERT INTO "public"."user_roles" VALUES (2, 3);
INSERT INTO "public"."user_roles" VALUES (3, 1);
INSERT INTO "public"."user_roles" VALUES (4, 2);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS "public"."users";
CREATE TABLE "public"."users" (
  "id" int8 NOT NULL DEFAULT nextval('users_id_seq'::regclass),
  "email" varchar(50) COLLATE "pg_catalog"."default",
  "password" varchar(120) COLLATE "pg_catalog"."default",
  "username" varchar(20) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO "public"."users" VALUES (2, 'dosen@padepokan79.com', '$2a$10$UmncNp6/V7KMlp0qh71hCe7C3m7o4FwB7qpT3IOA1AJtdRGzpqbDW', 'dosen');
INSERT INTO "public"."users" VALUES (3, 'mahasiswa@padepokan79.com', '$2a$10$o/3CnHko1ytNV.ppt4G0luuxwenpbYK3ZyV2ew/YsuNEWdAYYDtum', 'mahasiswa');
INSERT INTO "public"."users" VALUES (4, 'admin@padepokan79.com', '$2a$10$Mjbj2qV4Ii7StXYGnOez0.vkc0RicOCuh0uUYDccq/ODp9XpSnJdS', 'admin');

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."roles_id_seq"
OWNED BY "public"."roles"."id";
SELECT setval('"public"."roles_id_seq"', 2, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."users_id_seq"
OWNED BY "public"."users"."id";
SELECT setval('"public"."users_id_seq"', 5, true);

-- ----------------------------
-- Primary Key structure for table data_nilai
-- ----------------------------
ALTER TABLE "public"."data_nilai" ADD CONSTRAINT "data_nilai_pkey" PRIMARY KEY ("dosen_id", "mata_kuliah_id", "nim");

-- ----------------------------
-- Foreign Keys structure for table data_nilai
-- ----------------------------
ALTER TABLE "public"."data_nilai" ADD CONSTRAINT "fk9yf2143og52omis6cvf7tw0ln" FOREIGN KEY ("nim") REFERENCES "public"."mahasiswa" ("nim") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."data_nilai" ADD CONSTRAINT "fkknl87q3eiitb0t0y5h51nav6g" FOREIGN KEY ("mata_kuliah_id") REFERENCES "public"."mata_kuliah" ("mata_kuliah_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."data_nilai" ADD CONSTRAINT "fktgxgbmo01iid9lw7s8i0mpajy" FOREIGN KEY ("dosen_id") REFERENCES "public"."dosen" ("dosen_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Primary Key structure for table dosen
-- ----------------------------
ALTER TABLE "public"."dosen" ADD CONSTRAINT "dosen_pkey" PRIMARY KEY ("dosen_id");

-- ----------------------------
-- Foreign Keys structure for table dosen
-- ----------------------------
ALTER TABLE "public"."dosen" ADD CONSTRAINT "fkife2ohmqmtors2uysebaa2td5" FOREIGN KEY ("user_id") REFERENCES "public"."users" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Primary Key structure for table mahasiswa
-- ----------------------------
ALTER TABLE "public"."mahasiswa" ADD CONSTRAINT "mahasiswa_pkey" PRIMARY KEY ("nim");

-- ----------------------------
-- Foreign Keys structure for table mahasiswa
-- ----------------------------
ALTER TABLE "public"."mahasiswa" ADD CONSTRAINT "fk5pqlohrl6plp7qhiug2a8hayg" FOREIGN KEY ("user_id") REFERENCES "public"."users" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Primary Key structure for table mata_kuliah
-- ----------------------------
ALTER TABLE "public"."mata_kuliah" ADD CONSTRAINT "mata_kuliah_pkey" PRIMARY KEY ("mata_kuliah_id");

-- ----------------------------
-- Foreign Keys structure for table mata_kuliah
-- ----------------------------
ALTER TABLE "public"."mata_kuliah" ADD CONSTRAINT "fko3wpj7g9lt1o6c6akpy7nionw" FOREIGN KEY ("nim") REFERENCES "public"."mahasiswa" ("nim") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Primary Key structure for table roles
-- ----------------------------
ALTER TABLE "public"."roles" ADD CONSTRAINT "roles_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table user_roles
-- ----------------------------
ALTER TABLE "public"."user_roles" ADD CONSTRAINT "user_roles_pkey" PRIMARY KEY ("user_id", "role_id");

-- ----------------------------
-- Foreign Keys structure for table user_roles
-- ----------------------------
ALTER TABLE "public"."user_roles" ADD CONSTRAINT "fkh8ciramu9cc9q3qcqiv4ue8a6" FOREIGN KEY ("role_id") REFERENCES "public"."roles" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."user_roles" ADD CONSTRAINT "fkhfh9dx7w3ubf1co1vdev94g3f" FOREIGN KEY ("user_id") REFERENCES "public"."users" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Uniques structure for table users
-- ----------------------------
ALTER TABLE "public"."users" ADD CONSTRAINT "ukr43af9ap4edm43mmtq01oddj6" UNIQUE ("username");
ALTER TABLE "public"."users" ADD CONSTRAINT "uk6dotkott2kjsp8vw4d0m25fb7" UNIQUE ("email");

-- ----------------------------
-- Primary Key structure for table users
-- ----------------------------
ALTER TABLE "public"."users" ADD CONSTRAINT "users_pkey" PRIMARY KEY ("id");
