package com.example.akademisamuel.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.akademisamuel.models.User;


@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	@Query(
			value = "SELECT * FROM users WHERE username = :name",
			nativeQuery = true)
			User findByUname(
			@Param("name") String name);
	
  Optional<User> findByUsername(String username);

  Boolean existsByUsername(String username);

  Boolean existsByEmail(String email);
}
