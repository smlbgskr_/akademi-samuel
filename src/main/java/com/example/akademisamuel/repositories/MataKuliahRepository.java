package com.example.akademisamuel.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.akademisamuel.models.MataKuliah;

@Repository
public interface MataKuliahRepository extends JpaRepository<MataKuliah, Long>{

}
