package com.example.akademisamuel.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.akademisamuel.models.DataNilai;
import com.example.akademisamuel.models.DataNilaiId;

@Repository
public interface DataNilaiRepository extends JpaRepository<DataNilai, DataNilaiId>{

}
