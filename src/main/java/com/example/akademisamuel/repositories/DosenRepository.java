package com.example.akademisamuel.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.akademisamuel.models.Dosen;

@Repository
public interface DosenRepository extends JpaRepository<Dosen, Long>{

}
