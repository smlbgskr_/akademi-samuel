package com.example.akademisamuel.controllers;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.akademisamuel.models.Dosen;
import com.example.akademisamuel.models.ERole;
import com.example.akademisamuel.models.Mahasiswa;
import com.example.akademisamuel.models.Role;
import com.example.akademisamuel.models.User;
import com.example.akademisamuel.payload.request.LoginRequest;
import com.example.akademisamuel.payload.request.SignupRequest;
import com.example.akademisamuel.payload.response.JwtResponse;
import com.example.akademisamuel.payload.response.MessageResponse;
import com.example.akademisamuel.repositories.DosenRepository;
import com.example.akademisamuel.repositories.MahasiswaRepository;
import com.example.akademisamuel.repositories.RoleRepository;
import com.example.akademisamuel.repositories.UserRepository;
import com.example.akademisamuel.security.jwt.JwtUtils;
import com.example.akademisamuel.security.services.UserDetailsImpl;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
  @Autowired
  AuthenticationManager authenticationManager;

  @Autowired
  UserRepository userRepository;

  @Autowired
  RoleRepository roleRepository;
  
  @Autowired
  MahasiswaRepository mhsRepository;
  
  @Autowired
  DosenRepository dosenRepository;

  @Autowired
  PasswordEncoder encoder;

  @Autowired
  JwtUtils jwtUtils;

  @PostMapping("/signin")
  public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

    Authentication authentication = authenticationManager.authenticate(
        new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

    SecurityContextHolder.getContext().setAuthentication(authentication);
    String jwt = jwtUtils.generateJwtToken(authentication);
    
    UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();    
    List<String> roles = userDetails.getAuthorities().stream()
        .map(item -> item.getAuthority())
        .collect(Collectors.toList());

    return ResponseEntity.ok(new JwtResponse(jwt, 
                         userDetails.getId(), 
                         userDetails.getUsername(), 
                         userDetails.getEmail(), 
                         roles));
  }

  @PostMapping("/signup")
  public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
    if (userRepository.existsByUsername(signUpRequest.getUsername())) {
      return ResponseEntity
          .badRequest()
          .body(new MessageResponse("Error: Username is already taken!"));
    }

    if (userRepository.existsByEmail(signUpRequest.getEmail())) {
      return ResponseEntity
          .badRequest()
          .body(new MessageResponse("Error: Email is already in use!"));
    }

    // Create new user's account
    User user = new User(signUpRequest.getUsername(), 
               signUpRequest.getEmail(),
               encoder.encode(signUpRequest.getPassword()));

    Set<String> strRoles = signUpRequest.getRole();
    Set<Role> roles = new HashSet<>();

    if (strRoles == null) {
      Role mahasiswaRole = roleRepository.findByName(ERole.ROLE_MAHASISWA)
          .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
      roles.add(mahasiswaRole);
    } else {
      strRoles.forEach(role -> {
        switch (role) {
        case "dosen":
          Role dosenRole = roleRepository.findByName(ERole.ROLE_DOSEN)
              .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
          roles.add(dosenRole);

          break;
        case "admin":
          Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
              .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
          roles.add(adminRole);

          break;
        default:
          Role mahasiswaRole = roleRepository.findByName(ERole.ROLE_MAHASISWA)
              .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
          roles.add(mahasiswaRole);
        }
      });
    }
    
    Long id;
    Mahasiswa mahasiswa = new Mahasiswa();
    Dosen dosen = new Dosen();

    user.setRoles(roles);
    userRepository.save(user);
    
    User userData = userRepository.findByUname(signUpRequest.getUsername());
    id = userData.getId();
    String roleUser = "";
    for (Role role2 : userData.getRoles()) {
    	roleUser = role2.getName().toString();
        if (roleUser.equalsIgnoreCase("ROLE_MAHASISWA")) {
        	mahasiswa.setUser(userData);
			mahasiswa.setNim(id);
			mhsRepository.save(mahasiswa);
		} else if (roleUser.equalsIgnoreCase("ROLE_DOSEN")) {
			dosen.setUser(userData);
			dosen.setDosenId(id);
			dosenRepository.save(dosen);
		}
	}

    return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
  }
  
  
}
