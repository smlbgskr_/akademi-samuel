package com.example.akademisamuel.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.akademisamuel.helpers.ExcelMahasiswaHelper;
import com.example.akademisamuel.models.Mahasiswa;
import com.example.akademisamuel.payload.response.MessageResponse;
import com.example.akademisamuel.services.ExcelMahasiswaService;

@RestController
@RequestMapping("/api/excel")
public class ExcelMahasiswaControllers {

	@Autowired
	ExcelMahasiswaService fileService;
	
	@PreAuthorize("hasRole('DOSEN')")
	@PostMapping("/upload")
	public ResponseEntity<MessageResponse> uploadFile(@RequestParam("file") MultipartFile file){
		String message = "";
				 
		if (ExcelMahasiswaHelper.hasExcelFormat(file)) {
			try {
				fileService.save(file);
				message = "Uploaded file succesfully : " + file.getOriginalFilename();
				return ResponseEntity.status(HttpStatus.OK).body(new MessageResponse(message));
			} catch (Exception e) {
				message = "Could not upload the file : " + file.getOriginalFilename();
				return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new MessageResponse(message));
			}
		}
		
		message = "Unauthorized";
		return ResponseEntity.status(HttpStatus.FORBIDDEN).body(new MessageResponse(message));
	}
	
	@GetMapping("/mahasiswa")
	public ResponseEntity<List<Mahasiswa>> getAllMahasiswas(){
		try {
			List<Mahasiswa> listMahasiswa = fileService.getAllMahasiswas();
			if (listMahasiswa.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<>(listMahasiswa, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}
