package com.example.akademisamuel.helpers;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import com.example.akademisamuel.models.Mahasiswa;

public class ExcelMahasiswaHelper {
	
	 public static String TYPE = "text/xlsx";
	  static String[] HEADERs = { "nim", "alamat", "jurusan", "nama_mahasiswa", "tanggal_lahir"};
	  static String SHEET = "Mahasiswa";
	  public static boolean hasExcelFormat(MultipartFile file) {
//	    if (!TYPE.equals(file.getContentType())) {
//	      return false;
//	    }
	    return true;
	  }
	  public static List<Mahasiswa> excelToMahasiswa(InputStream is) {
	    try {
	      Workbook workbook = new XSSFWorkbook(is);
	      Sheet sheet = workbook.getSheet(SHEET);
	      Iterator<Row> rows = sheet.iterator();
	      List<Mahasiswa> listMahasiswa = new ArrayList<Mahasiswa>();
	      int rowNumber = 0;
	      while (rows.hasNext()) {
	        Row currentRow = rows.next();
	        // skip header
	        if (rowNumber == 0) {
	          rowNumber++;
	          continue;
	        }
	        Iterator<Cell> cellsInRow = currentRow.iterator();
	        Mahasiswa mahasiswa = new Mahasiswa();
	        int cellIdx = 0;
	        while (cellsInRow.hasNext()) {
	          Cell currentCell = cellsInRow.next();
	          switch (cellIdx) {
	          case 0:
	        	// nim
	            mahasiswa.setNim((long) currentCell.getNumericCellValue());
	            break;
	          case 1:
	        	// alamat
	        	mahasiswa.setAlamat(currentCell.getStringCellValue());
	            break;
	          case 2:
	        	// jurusan
	        	mahasiswa.setJurusan(currentCell.getStringCellValue());
	            break;
	          case 3:
	        	// nama_mahasiswa
	            mahasiswa.setNamaMahasiswa(currentCell.getStringCellValue());
	            break;
	          case 4:
	        	// tanggal lahir
	        	mahasiswa.setTanggalLahir(currentCell.getDateCellValue());
	          default:
	            break;
	          }
	          cellIdx++;
	        }
	        listMahasiswa.add(mahasiswa);
	      }
	      workbook.close();
	      return listMahasiswa;
	    } catch (IOException e) {
	      throw new RuntimeException("fail to parse Excel file: " + e.getMessage());
	    }
	  }
	}