package com.example.akademisamuel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AkademiSamuelApplication {

	public static void main(String[] args) {
		SpringApplication.run(AkademiSamuelApplication.class, args);
	}

}
