package com.example.akademisamuel.models;

public enum ERole {
  ROLE_MAHASISWA,
  ROLE_ADMIN,
  ROLE_DOSEN
}
