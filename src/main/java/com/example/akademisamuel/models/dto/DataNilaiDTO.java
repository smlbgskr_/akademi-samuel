package com.example.akademisamuel.models.dto;

import com.example.akademisamuel.models.DataNilaiId;

public class DataNilaiDTO {

	private DataNilaiId id;
	private DosenDTO dosen;
	private MahasiswaDTO mahasiswa;
	private MataKuliahDTO mataKuliah;
	private Integer nilai;
	private String keterangan;
	
	public DataNilaiDTO() {
		// TODO Auto-generated constructor stub
	}

	public DataNilaiDTO(DataNilaiId id, DosenDTO dosen, MahasiswaDTO mahasiswa, MataKuliahDTO mataKuliah, Integer nilai,
			String keterangan) {
		this.id = id;
		this.dosen = dosen;
		this.mahasiswa = mahasiswa;
		this.mataKuliah = mataKuliah;
		this.nilai = nilai;
		this.keterangan = keterangan;
	}

	public DataNilaiId getId() {
		return id;
	}

	public void setId(DataNilaiId id) {
		this.id = id;
	}

	public DosenDTO getDosen() {
		return dosen;
	}

	public void setDosen(DosenDTO dosen) {
		this.dosen = dosen;
	}

	public MahasiswaDTO getMahasiswa() {
		return mahasiswa;
	}

	public void setMahasiswa(MahasiswaDTO mahasiswa) {
		this.mahasiswa = mahasiswa;
	}

	public MataKuliahDTO getMataKuliah() {
		return mataKuliah;
	}

	public void setMataKuliah(MataKuliahDTO mataKuliah) {
		this.mataKuliah = mataKuliah;
	}

	public Integer getNilai() {
		return nilai;
	}

	public void setNilai(Integer nilai) {
		this.nilai = nilai;
	}

	public String getKeterangan() {
		return keterangan;
	}

	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}
	
}
