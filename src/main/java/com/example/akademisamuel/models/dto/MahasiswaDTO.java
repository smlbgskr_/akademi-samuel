package com.example.akademisamuel.models.dto;

import java.util.Date;

public class MahasiswaDTO {

	private long nim;
	private String namaMahasiswa;
	private String alamat;
	private Date tanggalLahir;
	private String jurusan;
	
	public MahasiswaDTO() {
		// TODO Auto-generated constructor stub
	}

	public MahasiswaDTO(long nim, String namaMahasiswa, String alamat, Date tanggalLahir, String jurusan) {
		this.nim = nim;
		this.namaMahasiswa = namaMahasiswa;
		this.alamat = alamat;
		this.tanggalLahir = tanggalLahir;
		this.jurusan = jurusan;
	}

	public long getNim() {
		return nim;
	}

	public void setNim(long nim) {
		this.nim = nim;
	}

	public String getNamaMahasiswa() {
		return namaMahasiswa;
	}

	public void setNamaMahasiswa(String namaMahasiswa) {
		this.namaMahasiswa = namaMahasiswa;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public Date getTanggalLahir() {
		return tanggalLahir;
	}

	public void setTanggalLahir(Date tanggalLahir) {
		this.tanggalLahir = tanggalLahir;
	}

	public String getJurusan() {
		return jurusan;
	}

	public void setJurusan(String jurusan) {
		this.jurusan = jurusan;
	}
	
}
