package com.example.akademisamuel.models.dto;

public class DosenDTO {

	private long dosenId;
	private String namaDosen;
	
	public DosenDTO() {
		// TODO Auto-generated constructor stub
	}

	public DosenDTO(long dosenId, String namaDosen) {
		this.dosenId = dosenId;
		this.namaDosen = namaDosen;
	}

	public long getDosenId() {
		return dosenId;
	}

	public void setDosenId(long dosenId) {
		this.dosenId = dosenId;
	}

	public String getNamaDosen() {
		return namaDosen;
	}

	public void setNamaDosen(String namaDosen) {
		this.namaDosen = namaDosen;
	}
	
}
