package com.example.akademisamuel.models.dto;

import com.example.akademisamuel.models.Mahasiswa;

public class MataKuliahDTO {
	
	private long mataKuliahId;
	private MahasiswaDTO mahasiswa;
	private String namaMataKuliah;
	
	public MataKuliahDTO() {
		// TODO Auto-generated constructor stub
	}

	public MataKuliahDTO(long mataKuliahId, MahasiswaDTO mahasiswa, String namaMataKuliah) {
		this.mataKuliahId = mataKuliahId;
		this.mahasiswa = mahasiswa;
		this.namaMataKuliah = namaMataKuliah;
	}

	public long getMataKuliahId() {
		return mataKuliahId;
	}

	public void setMataKuliahId(long mataKuliahId) {
		this.mataKuliahId = mataKuliahId;
	}

	public MahasiswaDTO getMahasiswa() {
		return mahasiswa;
	}

	public void setMahasiswa(MahasiswaDTO mahasiswa) {
		this.mahasiswa = mahasiswa;
	}

	public String getNamaMataKuliah() {
		return namaMataKuliah;
	}

	public void setNamaMataKuliah(String namaMataKuliah) {
		this.namaMataKuliah = namaMataKuliah;
	}

}
