package com.example.akademisamuel.models;
// Generated Jul 1, 2022, 10:27:07 AM by Hibernate Tools 4.3.6.Final

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Dosen generated by hbm2java
 */
@Entity
@Table(name = "dosen", schema = "public")
public class Dosen implements java.io.Serializable {

	private long dosenId;
	private String namaDosen;
	private User user;
	private Set<DataNilai> dataNilais = new HashSet<DataNilai>(0);

	public Dosen() {
	}
	
	public Dosen(long dosenId, String namaDosen, User user, Set<DataNilai> dataNilais) {
		this.dosenId = dosenId;
		this.namaDosen = namaDosen;
		this.user = user;
		this.dataNilais = dataNilais;
	}

	@Id

	@Column(name = "dosen_id", unique = true, nullable = false)
	public long getDosenId() {
		return this.dosenId;
	}

	public void setDosenId(long dosenId) {
		this.dosenId = dosenId;
	}

	@Column(name = "nama_dosen", nullable = false)
	public String getNamaDosen() {
		return this.namaDosen;
	}

	public void setNamaDosen(String namaDosen) {
		this.namaDosen = namaDosen;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "dosen")
	public Set<DataNilai> getDataNilais() {
		return this.dataNilais;
	}

	public void setDataNilais(Set<DataNilai> dataNilais) {
		this.dataNilais = dataNilais;
	}
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
