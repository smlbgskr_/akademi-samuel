package com.example.akademisamuel.services;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.example.akademisamuel.helpers.ExcelMahasiswaHelper;
import com.example.akademisamuel.models.Mahasiswa;
import com.example.akademisamuel.repositories.MahasiswaRepository;

@Service
public class ExcelMahasiswaService {

	@Autowired
	MahasiswaRepository mhsRepo;
	
	public void save(MultipartFile file) {
		try {
			List<Mahasiswa> listMahasiswa = ExcelMahasiswaHelper.excelToMahasiswa(file.getInputStream());
			mhsRepo.saveAll(listMahasiswa);
		} catch (IOException e) {
			throw new RuntimeException("Fail to store excel data : " + e.getMessage());
		}	
	}
	
	public List<Mahasiswa> getAllMahasiswas(){
		return mhsRepo.findAll();
	}
	
}
